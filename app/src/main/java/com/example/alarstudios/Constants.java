package com.example.alarstudios;

public class Constants {

    public static final String USERNAME = "usernme";
    public static final String PASSWORD = "password";
    public static final String CODE = "code";
    public static final int PAGE_SIZE = 10;
    public static final String STATUS_OK = "ok";
    public static final String STATUS_ERROR = "error";

    public static final String DATA_ITEM = "data_item";
}
