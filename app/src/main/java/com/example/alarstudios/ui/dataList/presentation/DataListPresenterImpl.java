package com.example.alarstudios.ui.dataList.presentation;

import com.example.alarstudios.Constants;
import com.example.alarstudios.api.Api;
import com.example.alarstudios.api.CodeProvider;
import com.example.alarstudios.data.dto.DataItem;
import com.example.alarstudios.ui.dataList.adapter.DataListItemLister;
import com.example.alarstudios.ui.dataList.adapter.DataListItemView;
import com.example.alarstudios.ui.dataList.view.DataListView;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DataListPresenterImpl implements DataListPresenter, DataListItemLister {
    private DataListView view;
    Api api;
    ArrayList<DataItem> dataItems = new ArrayList<>();
    CompositeDisposable disposable = new CompositeDisposable();
    CodeProvider codeProvider;
    private int lastDataSetSize;

    public DataListPresenterImpl(Api api, CodeProvider codeProvider) {
        this.api = api;
        this.codeProvider = codeProvider;

    }

    @Override
    public void releaseView() {
        view = null;
    }

    @Override
    public void start() {
        if (!codeProvider.getCode().equalsIgnoreCase("")){
            loadData(1);
        }else {
            view.logout();
        }
    }

    private void loadData(int page) {
        disposable.add(api.getData(codeProvider.getCode(), page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        listDataWrapper -> {
                            view.onLoaded();
                            lastDataSetSize = listDataWrapper.getData().size();
                            if (listDataWrapper.getStatus().equalsIgnoreCase(Constants.STATUS_OK)) {
                                dataItems.addAll(listDataWrapper.getData());
                                view.notifyItemsLoaded(dataItems.size());
                            }
                        },throwable -> {
                            view.onNetworkError(throwable.getMessage());
                        }
                ));
    }

    @Override
    public void stop() {
        disposable.dispose();
        disposable.clear();
    }


    @Override
    public void bindView(DataListView view) {
        this.view = view;
    }

    @Override
    public void bindItemView(DataListItemView view, int position) {
        view.bind(dataItems.get(position));
        view.setListener(this);
    }

    @Override
    public int itemCount() {
        return dataItems.size();
    }

    @Override
    public void loadNext(int page) {
        loadData(page);
    }

    @Override
    public int getLastItemSetCount() {
        return lastDataSetSize;
    }


    @Override
    public void onClick(DataItem dataItem) {
        view.openItemFragment(dataItem);
    }
}
