package com.example.alarstudios.ui.auth.view;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.alarstudios.R;
import com.example.alarstudios.api.ApiFactory;
import com.example.alarstudios.api.CodeProvider;
import com.example.alarstudios.ui.BaseFragment;
import com.example.alarstudios.ui.auth.presentation.AuthFragmentPresenter;
import com.example.alarstudios.ui.auth.presentation.AuthFragmentPresenterImpl;
import com.example.alarstudios.ui.dataList.view.DataListFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.lang.ref.WeakReference;

public class AuthFragment extends BaseFragment implements AuthFragmentView {
    private AuthFragmentPresenter presenter;
    private CodeProvider codeProvider;
    private Button btnLogin;
    private TextInputEditText etLogin, etPassword;
    private TextInputLayout lLogin, lPassword;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_auth, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        codeProvider = new CodeProvider(new WeakReference(view.getContext()));
        initPresenter();
        setViewFunc();
    }

    @Override
    public void initViews(View view) {
        btnLogin = view.findViewById(R.id.btnLogin);
        etLogin = view.findViewById(R.id.etLogin);
        etPassword = view.findViewById(R.id.etPassword);
        lLogin = view.findViewById(R.id.lLogin);
        lPassword = view.findViewById(R.id.lPassword);

    }

    private void setViewFunc() {
        btnLogin.setOnClickListener(v -> presenter.onLoginClick(etLogin.getText().toString(), etPassword.getText().toString()));
        etLogin.addTextChangedListener(textWatcher);
        etPassword.addTextChangedListener(textWatcher);
    }

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (etPassword.getText().toString().length() > 0 && etLogin.getText().toString().length() > 0) {
                btnLogin.setEnabled(true);
            } else {
                btnLogin.setEnabled(false);
            }
            lPassword.setError(null);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    private void initPresenter() {
        presenter = new AuthFragmentPresenterImpl(ApiFactory.createApi(codeProvider), codeProvider);
        presenter.bindView(this);
    }

    @Override
    public void onAuth() {
        openListFragment();
    }

    @Override
    public void onError(String error) {
        lPassword.setError(error);
    }

    @Override
    public void onNetworkError(String message) {
        showErrorDialog(message);
    }

    private void openListFragment() {
        Fragment fragment = new DataListFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit();
    }
}
