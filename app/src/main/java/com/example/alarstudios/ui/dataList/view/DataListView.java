package com.example.alarstudios.ui.dataList.view;

import com.example.alarstudios.BaseView;
import com.example.alarstudios.data.dto.DataItem;

public interface DataListView extends BaseView {

    void openItemFragment(DataItem dataItem);

    void onLoaded();

    void notifyItemsLoaded(int size);


    void onNetworkError(String message);


}
