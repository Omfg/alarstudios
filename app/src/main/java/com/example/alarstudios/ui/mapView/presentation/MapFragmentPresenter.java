package com.example.alarstudios.ui.mapView.presentation;

import com.example.alarstudios.BasePresenter;
import com.example.alarstudios.ui.mapView.view.MapFragmentView;

public interface MapFragmentPresenter extends BasePresenter {

    void bindView(MapFragmentView view);
}
