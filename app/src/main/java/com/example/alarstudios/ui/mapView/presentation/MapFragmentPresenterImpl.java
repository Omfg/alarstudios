package com.example.alarstudios.ui.mapView.presentation;

import com.example.alarstudios.api.CodeProvider;
import com.example.alarstudios.ui.mapView.view.MapFragmentView;

public class MapFragmentPresenterImpl implements MapFragmentPresenter {
    MapFragmentView view;
    CodeProvider codeProvider;

    public MapFragmentPresenterImpl(CodeProvider codeProvider) {
        this.codeProvider = codeProvider;
    }

    @Override
    public void releaseView() {
        view=null;
    }

    @Override
    public void start() {
        if (codeProvider.getCode().equalsIgnoreCase("")){
            view.logout();
        }
    }

    @Override
    public void stop() {

    }

    @Override
    public void bindView(MapFragmentView view) {
        this.view = view;
    }
}
