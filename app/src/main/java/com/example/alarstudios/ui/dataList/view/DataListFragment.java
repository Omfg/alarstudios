package com.example.alarstudios.ui.dataList.view;

import android.app.admin.FactoryResetProtectionPolicy;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alarstudios.Constants;
import com.example.alarstudios.R;
import com.example.alarstudios.api.ApiFactory;
import com.example.alarstudios.api.CodeProvider;
import com.example.alarstudios.data.dto.DataItem;
import com.example.alarstudios.ui.BaseToolbarFragment;
import com.example.alarstudios.ui.auth.view.AuthFragment;
import com.example.alarstudios.ui.dataList.adapter.DataLIstAdapter;
import com.example.alarstudios.ui.dataList.presentation.DataListPresenter;
import com.example.alarstudios.ui.dataList.presentation.DataListPresenterImpl;
import com.example.alarstudios.ui.mapView.view.MapFragment;
import com.example.alarstudios.utils.PaginationScrollListener;

import java.lang.ref.WeakReference;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

public class DataListFragment extends BaseToolbarFragment implements DataListView {

    RecyclerView recyclerView;
    DataListPresenter presenter;
    CodeProvider codeProvider;
    DataLIstAdapter adapter;
    private int currentPage = 1;
    boolean isLoading = false;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_data_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        codeProvider = new CodeProvider(new WeakReference(getContext()));
        initPresenter();
        createAdapter();
        setPagination();
    }

    @Override
    public void initViews(View view) {
        super.initViews(view);
        recyclerView = view.findViewById(R.id.rvDataList);
    }


    private void createAdapter() {
        adapter = new DataLIstAdapter(presenter);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    private void setPagination() {
        recyclerView.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) recyclerView.getLayoutManager()) {
            @Override
            protected void loadItems() {
                if (!isLast() && !isLoading()) {
                    isLoading = true;
                    presenter.loadNext(currentPage++);
                } else {
                    isLast();
                }
            }

            @Override
            public boolean isLast() {
                if (presenter.getLastItemSetCount() < PAGE_SIZE) {
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void initPresenter() {
        presenter = new DataListPresenterImpl(ApiFactory.createApi(codeProvider), codeProvider);
        presenter.bindView(this);
        presenter.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.releaseView();
    }

    @Override
    public void openItemFragment(DataItem dataItem) {
        Fragment fragment = new MapFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.DATA_ITEM, dataItem);
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .addToBackStack("MapFragment")
                .commit();
    }

    @Override
    public void onLoaded() {
        isLoading = false;
    }

    @Override
    public void notifyItemsLoaded(int size) {
        adapter.notifyItemRangeInserted(size - 10, size);
    }


    @Override
    public void goBack() {
        super.logout();
    }


    @Override
    public void onNetworkError(String message) {
        isLoading = false;
        showErrorDialog(message);
    }
}
