package com.example.alarstudios.ui.auth.presentation;


import android.view.View;

import com.example.alarstudios.api.Api;
import com.example.alarstudios.api.CodeProvider;
import com.example.alarstudios.data.dto.AuthDto;
import com.example.alarstudios.ui.auth.view.AuthFragment;
import com.example.alarstudios.ui.auth.view.AuthFragmentView;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class AuthFragmentPresenterImpl implements AuthFragmentPresenter {

    AuthFragmentView view;
    CompositeDisposable disposable;
    Api api;
    CodeProvider codeProvider;

    public AuthFragmentPresenterImpl(Api api, CodeProvider codeProvider) {
        this.api = api;
        this.codeProvider = codeProvider;
        this.disposable = new CompositeDisposable();
    }

    @Override
    public void bindView(AuthFragmentView view) {
        this.view = view;
    }

    @Override
    public void releaseView() {
        this.view = null;
        disposable.dispose();
    }

    @Override
    public void start() {

    }


    @Override
    public void stop() {

    }

    @Override
    public void onLoginClick(String userName, String password) {
        disposable.add(api.authenticate(userName, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(authDto -> {
                    if (authDto.getStatus().equalsIgnoreCase("ok")) {
                        codeProvider.saveCode(authDto.getCode());
                        view.onAuth();
                    } else {
                        view.onError("Authentication Error. Wrong Username or password");
                    }

                }, throwable -> {
                    view.onNetworkError(throwable.getMessage());
                }));
    }
}
