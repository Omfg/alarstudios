package com.example.alarstudios.ui.dataList.presentation;

import com.example.alarstudios.BasePresenter;
import com.example.alarstudios.ui.dataList.adapter.DataListItemView;
import com.example.alarstudios.ui.dataList.view.DataListView;

public interface DataListPresenter extends BasePresenter {

    void bindView(DataListView view);

    void bindItemView(DataListItemView view, int position);

    int itemCount();

    void loadNext(int page);

    int getLastItemSetCount();



}
