package com.example.alarstudios.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.alarstudios.R;
import com.example.alarstudios.api.CodeProvider;
import com.example.alarstudios.ui.auth.view.AuthFragment;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.lang.ref.WeakReference;

public abstract class BaseFragment extends Fragment {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    protected abstract void initViews(View view);

    public void logout() {
        CodeProvider codeProvider = new CodeProvider(new WeakReference(getContext()));
        codeProvider.logout();
        Fragment fragment = new AuthFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit();
    }

    public void showErrorDialog(String msg) {
        BottomSheetDialog dialog = new BottomSheetDialog(getContext());
        View view = getLayoutInflater().inflate(R.layout.error_view, null);
        TextView title = view.findViewById(R.id.tvTitle);
        TextView tvMessage = view.findViewById(R.id.tvMsg);
        title.setText(getString(R.string.error));
        tvMessage.setText(msg);
        dialog.setContentView(view);
        dialog.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 2000);
            }
        });
        dialog.show();
    }
}
