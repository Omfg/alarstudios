package com.example.alarstudios.ui.dataList.adapter;

import com.example.alarstudios.data.dto.DataItem;

public interface DataListItemLister {
    void onClick(DataItem dataItem);
}
