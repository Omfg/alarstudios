package com.example.alarstudios.ui.auth.view;

import com.example.alarstudios.BaseView;

public interface AuthFragmentView extends BaseView {
    void onAuth();

    void onError(String error);

    void onNetworkError(String message);

}
