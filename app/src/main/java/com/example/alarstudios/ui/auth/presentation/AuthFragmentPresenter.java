package com.example.alarstudios.ui.auth.presentation;


import com.example.alarstudios.BasePresenter;
import com.example.alarstudios.ui.auth.view.AuthFragmentView;

public interface  AuthFragmentPresenter extends BasePresenter {


    void bindView(AuthFragmentView view);

    void onLoginClick(String userName, String password);
}
