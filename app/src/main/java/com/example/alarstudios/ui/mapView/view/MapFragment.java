package com.example.alarstudios.ui.mapView.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.alarstudios.Constants;
import com.example.alarstudios.R;
import com.example.alarstudios.api.CodeProvider;
import com.example.alarstudios.data.dto.DataItem;
import com.example.alarstudios.ui.BaseToolbarFragment;
import com.example.alarstudios.ui.mapView.presentation.MapFragmentPresenter;
import com.example.alarstudios.ui.mapView.presentation.MapFragmentPresenterImpl;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.lang.ref.WeakReference;

public class MapFragment extends BaseToolbarFragment implements MapFragmentView {

    TextView tvId, tvName, tvCountry, tvLat, tvLon;
    MapView mapView;
    MapFragmentPresenter presenter;
    DataItem item;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mapview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        item = (DataItem) getArguments().getSerializable(Constants.DATA_ITEM);
        initView(view);
        initPresenter();
        bindData();
        bindMap(savedInstanceState);
    }

    private void initView(View view) {
        super.initViews(view);
        tvId = view.findViewById(R.id.tvId);
        tvName = view.findViewById(R.id.tvName);
        tvCountry = view.findViewById(R.id.tvCountry);
        tvLat = view.findViewById(R.id.tvLat);
        tvLon = view.findViewById(R.id.tvLon);
        mapView = view.findViewById(R.id.mapView);
    }

    private void initPresenter() {
        presenter = new MapFragmentPresenterImpl(new CodeProvider(new WeakReference(getContext())));
        presenter.bindView(this);
        presenter.start();
    }

    private void bindData() {
        toolbar.setTitle(item.getName());
        tvId.setText(getString(R.string.id, item.getId()));
        tvCountry.setText(getString(R.string.country, item.getCountry()));
        tvName.setText(getString(R.string.name, item.getName()));
        tvLat.setText(getString(R.string.lat, Double.toString(item.getLat())));
        tvLon.setText(getString(R.string.lon, Double.toString(item.getLon())));
    }

    private void bindMap(Bundle savedInstanceState) {
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(googleMap -> {
            googleMap.addMarker(new MarkerOptions().position(new LatLng(item.getLat(), item.getLon())).title(item.getName()));
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(item.getLat(), item.getLon()), 16f);
            googleMap.moveCamera(update);
        });
    }

    @Override
    public void logout() {
        super.logout();
    }

}
