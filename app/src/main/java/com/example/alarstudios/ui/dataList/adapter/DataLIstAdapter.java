package com.example.alarstudios.ui.dataList.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alarstudios.R;
import com.example.alarstudios.data.dto.DataItem;
import com.example.alarstudios.ui.dataList.presentation.DataListPresenter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DataLIstAdapter extends RecyclerView.Adapter<DataLIstAdapter.ViewHolder> {

    DataListPresenter presenter;

    public DataLIstAdapter(DataListPresenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        presenter.bindItemView(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.itemCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements DataListItemView {
        DataItem dataItem;
        ImageView imageView;
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void bind(DataItem dataItem) {
            this.dataItem = dataItem;
            imageView = itemView.findViewById(R.id.ivImage);
            textView = itemView.findViewById(R.id.tvName);
            textView.setText(dataItem.getName());
            Picasso.get().load(itemView.getContext().getString(R.string.sample_image)).into(imageView);
        }

        @Override
        public void setListener(DataListItemLister listener) {
            itemView.setOnClickListener(v -> {
                listener.onClick(dataItem);
            });
        }
    }
}
