package com.example.alarstudios.ui.dataList.adapter;

import com.example.alarstudios.data.dto.DataItem;

public interface DataListItemView {
    void bind(DataItem dataItem);

    void setListener(DataListItemLister listener);
}
