package com.example.alarstudios;

import android.view.View;

public interface BasePresenter {
    void releaseView();
    void start();
    void stop();
}
