package com.example.alarstudios;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.example.alarstudios.api.CodeProvider;
import com.example.alarstudios.ui.auth.view.AuthFragment;
import com.example.alarstudios.ui.dataList.view.DataListFragment;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CodeProvider codeProvider = new CodeProvider(new WeakReference(this));
        if (codeProvider.getCode().equalsIgnoreCase("")) {
            Fragment fragment = new AuthFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .commit();
        } else {
            Fragment fragment = new DataListFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .commit();

        }
    }
}