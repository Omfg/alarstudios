package com.example.alarstudios.api;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.alarstudios.BuildConfig;

import java.lang.ref.WeakReference;

import static com.example.alarstudios.Constants.CODE;
import static com.example.alarstudios.Constants.PASSWORD;
import static com.example.alarstudios.Constants.USERNAME;

public class CodeProvider {
    WeakReference<Context> context;


    public CodeProvider(WeakReference<Context> context) {
        this.context = context;
    }


    public void saveCode(String code) {
        SharedPreferences.Editor editor = context.get().getSharedPreferences(BuildConfig.SP_STORAGE, Context.MODE_PRIVATE).edit();
        editor.putString(CODE, code);
        editor.apply();
    }

    public String getCode() {
        return context.get().getSharedPreferences(BuildConfig.SP_STORAGE, Context.MODE_PRIVATE).getString(CODE,"");
    }

    public void logout(){
        SharedPreferences.Editor editor = context.get().getSharedPreferences(BuildConfig.SP_STORAGE, Context.MODE_PRIVATE).edit();
        editor.remove(CODE);
        editor.apply();
    }

}
