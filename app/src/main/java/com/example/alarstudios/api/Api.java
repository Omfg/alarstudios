package com.example.alarstudios.api;

import com.example.alarstudios.data.dto.AuthDto;
import com.example.alarstudios.data.dto.DataItem;
import com.example.alarstudios.data.dto.PaginationWrapper;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

    @GET("test/auth.cgi")
    Single<AuthDto> authenticate(@Query("username") String userName, @Query("password") String password);

    @GET("test/data.cgi")
    Single<PaginationWrapper<List<DataItem>>> getData(@Query("code") String code, @Query("page") int page);
}
