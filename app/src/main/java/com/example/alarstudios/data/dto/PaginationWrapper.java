package com.example.alarstudios.data.dto;

import com.google.gson.annotations.SerializedName;

public class PaginationWrapper<T> {
    @SerializedName("status")
    private String status;
    @SerializedName("page")
    private int page;
    @SerializedName("data")
    private T data;

    public String getStatus() {
        return status;
    }

    public int getPage() {
        return page;
    }

    public T getData() {
        return data;
    }


}
