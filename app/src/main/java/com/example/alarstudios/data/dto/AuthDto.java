package com.example.alarstudios.data.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthDto {
    @SerializedName("status")
    private String status;
    @SerializedName("code")
    @Expose
    private String code;

    public String getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }
}
