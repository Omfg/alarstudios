package com.example.alarstudios.data.post;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthPost {

    @SerializedName("username")
    @Expose
    private String userName;
    @SerializedName("password")
    @Expose
    private String password;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
