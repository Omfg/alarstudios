package com.example.alarstudios.utils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {
    LinearLayoutManager layoutManager;

    public PaginationScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);

        int visibleItems = layoutManager.getChildCount();
        int totalItems = layoutManager.getItemCount();
        int lastItem = layoutManager.findLastVisibleItemPosition();

        if (visibleItems+lastItem>=totalItems){
            loadItems();
        }

    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
    }

    protected abstract void loadItems();
    public abstract boolean isLast();
    public abstract boolean isLoading();
}
